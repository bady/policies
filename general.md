Saakhi Project

GENERAL POLICY STATEMENT
========================

## TABLE OF CONTENTS
1. [Primary Objective](#primary-objective)
    1. [Intellectual Property Rights (IPRs)](#intellectual-property-rights-iprs)
    2. [Crowd Sourcing](#crowd-sourcing)
2. [Membership Policy](#membership-policy)
    1. [Individuals and Entities](#individuals-and-entities)
    2. [Registration and Fee](#registration-and-fee)
3. [Monetary Policy](#monetary-policy)
4. [Technical Policy](#technical-policy)
    1. [Profesional Profiling](#professional-profiling)
    2. [User Accountability](#user-accountability)
    3. [Versioning](#versioning)
    4. [Communications Policy](#communications-policy)
5. [Version Details](#version-details)

## PRIMARY OBJECTIVE
The primary objective of Saakhi Project is to transform research into a collaborative community
process with sharing of expertise, open publishing, open peer reviews and public accountability.
Saakhi aims to become the platform that is the backbone of this evolution.

### Intellectual Property Rights (IPRs)
Community building and knowledge sharing are at the core of Saakhi's objectives. Saakhi will
promote the use of IPRs (patents, copyrights, etc) in ways that promote community building and
knowledge sharing. Their use in ways that hurt the community process will be discouraged.

### Crowd Sourcing
Saakhi aims to be a comprehensive platform for open research. And one important aspect of research
is funding. In due course, Saakhi will provide crowd sourcing features for researchers to raise
funding for their research projects. This will be subjected to [monetary policy](#monetary-policy)
described below.

## MEMBERSHIP POLICY

### Individuals and Entities
All memberships on the Sakhi platform will be exclusively for individuals. Individuals are free to
be associated with or endorsed by any entity. But there will be no membership for any other entity
(like companies) within the Sakhi platform. All work and communication on the platform will be
based on collaborative agreements between individuals.

### Registration and Fee
Registration of individual will be done on the basis of proof of identity, submission of verified
[OpenPGP key](#user-accountability) and a one-time registration fee. The proof of identity is
currently chosen to be *Indian voter's ID card*.

The registration fee is currently set at Rs.500. Saakhi however aims to be an inclusive platform
and wont reject participants if they are not able to pay the registration fee. For such applicants,
Saakhi core team may assign special contibutory task. On completion of that task, membership will
be given with fee waiver.

## MONETARY POLICY
Saakhi project will not be involved in any monetary transactions, other than the funds required for
development, maintainance, administration and promotion of the platform. The membership fees and
other contributions to Saakhi project will be used for the above mentioned tasks. All other monetary
transactions, including funding of research shall be handled by reputed third party agencies. Saakhi
may however, facilitate such transactions by providing a [platform](#crowd-sourcing) without being
directly involved in it.

## TECHNICAL POLICY

### Professional Profiling 
**(NEED A BETTER WORD THAN PROFILING - THIS ONE HAS NEGATIVE MEANING)**

All members will be profiled for technical and professional merit and proficiency in different
disciplines of expertise. This will be based on feedback from peers and automated profiling
procedures. The exact criteria for this metric and the procedure **will be** detailed in another
document.

### User Accountability
Accountability of researchers, reviewers and users is of atmost importance to research process.
Modern cryptographic tools are widely used in software development domains to ensure authenticity
and accountability of published software. Saakhi will use the same tools to ensure accountability
in open research. Digital signatures shall be used to ensure autheticity of documents.

The tool that will be used will be any implementation of the OpenPGP
([IETF RFC 4880](https://tools.ietf.org/html/rfc4880)) standard. A common free implementation of
the standard is [GnuPG](https://gnupg.org/). Saakhi membership applicants must have an OpenPGP key
and should know how to use the tool for tasks like digital signing, key verification etc. Saakhi
community members shall strive to popularize the tool by providing familiarization and training. 

### Versioning
Saakhi will provide ways to uniquely identify and reference research documents. This will also
include the ability to uniquely identify multiple versions of the same document. This will give
the author the freedom to continuously amend the document, while users will have no ambiguity as
to which version they are referring to.

### Communications Policy
Saakhi is a platform for open research and publications. All matters published on Saakhi will be
publically visible. Saakhi wont support personal or restricted messaging.

**Note:** This policy may be revisited or amended in the future. Integration with external
personal communication services may be considered as an option.

## VERSION DETAILS
```
Version      : 0.1.0 (Draft; Request For Comments)
Date         : 08 August 2017    [Initial Date: 08 August 2017]

Contributors : 
    - Gokul Das B

Reviewers    : 
```
This document outlines policies that guide the development and functioning of Saakhi Project as on
the date of publication. It may be amended in the future based on experiences and community
discussions, while keeping it compatible with the general principles expressed above.
